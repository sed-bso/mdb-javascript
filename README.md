# MDB-javascript

## Preparation

- instrall NodeJS on your computer
- clone this repository
- in the root folder of the cloned repo open a terminal and enter the command : `node server.js`. 
This run a server open to world on port 80.
In order to change the adress to local host edit the 5th line from `0.0.0.0` to `127.0.0.1`
In order to change the server port change the 6th line from `80` to anything you want
- processing documentation : https://p5js.org/reference/

## Coding something cool in processing

#### step 1 : particle

- each time the client click inside the processing window, we draw a circle at this position,
- each new circle have a lifetime wich is decresing and when it reach 0 the circle is disapearing.

**Tips**
- please keep your `function draw()` as clean as possible, so create a 'reduceLifeParticle' function
- processing is calling a callback `function mousePressed()`, each time a mouse pressed event occur
- when ever you want you can read the mouse position using the variables : `mouseX` and `mouseY`
- to create an array and fill it with values:

```javascript
var myarray = []
for(var i=0; i<10; i++)
{
    var element = { x:10, y:20 };
    myarray.push(element);
}
```

#### step 2 : gravity

- we add gravity to our simulation : each particle, in addition to their position, store now their speed,
- each new frame the speed on y axis is incremented by a gravity factor,
- each new frame the position is incremented by the particle speed,
- if the particle is exiting the window in height, we clamp the particle position to a valid one and we stop this particle movement

#### step 3 : bouncyness

- we store the value of the slider in a bouncyness variable,
- each time a particle collide the floor or a window border we reflect the speed depending on the boundary normal, and the new speed magnitude depend on our bouncyness variable,
- here is an example of the reflect function :

```javascript
// param v : a vector with a component x and y representing the vector to reflect
// param n : a vector with a component x and y representing the normal
function reflect(v, n)
{
    // v = v -2*dot(v, n)*n
    p = v.x*n.x + v.y*n.y;
    return { x: v.x - 2*p*n.x, y: v.y - 2*p*n.y };
}
```
