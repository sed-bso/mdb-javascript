var hexapodServerHttp = require('http').createServer(handler);
var url= require('url')
var fs = require('fs')

const adress = '0.0.0.0';
const port = 80;

// Http handler function
function handler (req, res)
{
    // Using URL to parse the requested URL
    var path = url.parse(req.url).pathname;
    
    // Managing the root route
    if (path == '/')
	{
        index = fs.readFile	(__dirname+'/public/mainPage.html', 
            function(error,data)
			{
                if (error) {
                    res.writeHead(500);
                    return res.end("Error: unable to load mainPage.html");
                }
                res.writeHead(200,{'Content-Type': 'text/html'});
                res.end(data);
            }
							);
    }
	// Catch security issue
	else if (path.indexOf("..") !== -1)
	{
		// This might be something like "../../../../etc/passwd".
		res.writeHead(403);
		res.end("");
		console.log(`Security alert !`);
	}
	// Managing the route for the javascript files
	else if( /\.(js)$/.test(path) )
	{
        index = fs.readFile	(__dirname+'/public'+path, 
            function(error,data)
			{
                if (error) {
                    res.writeHead(500);
                    return res.end("Error: unable to load " + path);
                }
                res.writeHead(200,{'Content-Type': 'text/plain'});
                res.end(data);
            }
							);
    }
	// Managing the route for the css files
	else if( /\.(css)$/.test(path) )
	{
        index = fs.readFile	(__dirname+'/public'+path, 
            function(error,data)
			{
                if (error)
				{
                    res.writeHead(500);
                    return res.end("Error: unable to load " + path);
                }
                res.writeHead(200,{'Content-Type': 'text/plain'});
                res.end(data);
            }
							);
    }
	// Try open standart page
	else
	{
        index = fs.readFile	(__dirname+'/public'+path, 
            function(error,data)
			{
                if (error) {
                    res.writeHead(404);
					return res.end("Error: 404 - File not found.");
                }
                res.writeHead(200,{'Content-Type': 'text/html'});
                res.end(data);
            }
							);
    }
}

// launch the server
hexapodServerHttp.listen(port, adress, () => {
	console.log(`Server running at http://${adress}:${port}/`);
});
