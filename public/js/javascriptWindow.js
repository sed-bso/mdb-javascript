
/*
	global attributes and variables
*/
var canvasContainer;
var slider, output;

/*
	setup functions called once for instanciation
*/
function setup()
{
	//	p5js and other stuf
	canvasContainer = document.getElementById('javascriptWindow');
	slider = document.getElementById("bouncynessRange");
	output = document.getElementById("bouncynessValue");
	slider.oninput = function() { output.innerHTML = 0.01*this.value; }
	output.innerHTML = 0.01*slider.value;
	var myCanvas = createCanvas(canvasContainer.clientWidth, canvasContainer.clientHeight);
	myCanvas.parent('javascriptWindow');
	
	//	init window
	frameRate(30);
}


/*
	callback functions : used by p5.js for user interactions
*/
function windowResized()
{
	resizeCanvas(canvasContainer.clientWidth, canvasContainer.clientHeight);
}

/*
	usefull
*/

/*
	updates 
*/

/*
	draw function called once per frame : 30 times by seconds
*/
function draw()
{
	// Begin frame
	clear();
	background(200);
}